IF OBJECT_ID('dbo.bbtcsp_GetCustomFieldsGlobalCodeCategory') IS NOT NULL
	DROP PROCEDURE dbo.bbtcsp_GetCustomFieldsGlobalCodeCategory
GO



CREATE PROCEDURE bbtcsp_GetCustomFieldsGlobalCodeCategory
AS
BEGIN

SELECT Category = CASE gcc.Category WHEN 'xServiceRecipients' THEN 'XSERVICERECIPIENTS'
                                       WHEN 'xModeOfDelivery' THEN 'XMODEOFDELIVERY'
                                       WHEN 'xCrisisType' THEN 'XCRISISTYPE'
                      end

, CategoryName = CASE gcc.Category WHEN 'xServiceRecipients' THEN 'Service Recipients'
                                       WHEN 'xModeOfDelivery' THEN 'Mode of Delivery'
                                       WHEN 'xCrisisType' THEN 'Crisis'
                      end

FROM GlobalCodeCategories AS gcc
WHERE gcc.Category IN ('xServiceRecipients', 'xModeOfDelivery', 'xCrisisType')

END

GO
