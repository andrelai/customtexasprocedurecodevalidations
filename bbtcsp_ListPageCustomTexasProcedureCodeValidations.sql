IF OBJECT_ID('dbo.bbtcsp_ListPageCustomTexasProcedureCodeValidations') IS NOT NULL
	DROP PROCEDURE dbo.bbtcsp_ListPageCustomTexasProcedureCodeValidations
GO



CREATE PROCEDURE [dbo].[bbtcsp_ListPageCustomTexasProcedureCodeValidations]
    @SessionId varchar(max),
    @InstanceId INT ,
    @PageNumber INT ,
    @PageSize INT ,
    @SortExpression VARCHAR(100),
	/*Other parameters*/
	@ProcedureCodeName VARCHAR(250),
	@Category VARCHAR(250)
AS /******************************************************************************
**		File: bbtcsp_ListPageCustomTexasProcedureCodeValidations.sql
**		Name: bbtcsp_ListPageCustomTexasProcedureCodeValidations
**		Desc: 
**
**		This template can be customized:
**              
**		Return values:
** 
**		Called by:   
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: alai
**		Date: 11/25/2019
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		    Author:				    Description:
**		--------		--------				-------------------------------------------
**	    11/25/2019          alai			    created template
*******************************************************************************/
    BEGIN            
 
        BEGIN TRY         
		          
            SET @ProcedureCodeName = NULLIF(@ProcedureCodeName, '')
            SET @Category = NULLIF(@Category,'')

            SET @SortExpression = RTRIM(LTRIM(@SortExpression));               
              
            IF ISNULL(@SortExpression , '') = ''
                BEGIN               
                    SET @SortExpression = 'ProcedureCodeName';               
                END;               
                     

			                    
            CREATE TABLE #RawData
                (
                  PrimaryKey INT
                  ,CategoryName nvarchar(50)
                  ,CodeName nvarchar(250)
                  ,ProcedureCodeName nvarchar(250)
                  ,DefaultValues VARCHAR(10)
                );      

            INSERT INTO #RawData (
              PrimaryKey
             ,CategoryName
             ,CodeName
             ,ProcedureCodeName
             ,DefaultValues)
            SELECT
              ctpcv.TexasProviderValidationId
             , CASE ctpcv.CategoryName WHEN 'xServiceRecipients' THEN 'Service Recipients'
                                       WHEN 'xModeOfDelivery' THEN 'Mode of Delivery'
                                       WHEN 'xCrisisType' THEN 'Crisis'
              end
             ,ctpcv.CodeName
             ,ctpcv.ProcedureCodeName
             ,CASE DefaultValues WHEN 'Y' THEN 'Yes' WHEN 'N' THEN 'No' ELSE '' END
			      FROM
				      CustomTexasProcedureCodeValidations AS ctpcv
			      WHERE
				      ISNULL(ctpcv.RecordDeleted, 'N') = 'N'
				      AND (ctpcv.ProcedureCodeName LIKE '%' + @ProcedureCodeName + '%' OR @ProcedureCodeName IS NULL)
				      AND (ctpcv.CategoryName LIKE '%' + @Category + '%' OR @Category IS NULL)
            ORDER BY 
              ctpcv.ProcedureCodeName, ctpcv.CategoryName
                        

            DECLARE @TotalRow INT;
            SELECT
                    @TotalRow = COUNT(*)
                FROM
                    #RawData AS rd;
                       

            CREATE TABLE #RankResultSet
                (
                  PrimaryKey INT
				          ,CategoryName nvarchar(50)
                  ,CodeName nvarchar(250)
                  ,ProcedureCodeName nvarchar(250)
                  ,DefaultValues VARCHAR(10)
                  ,TotalCount INT 
                  ,RowNumber INT
                );   
			 INSERT INTO #RankResultSet (
                PrimaryKey
               ,CategoryName
               ,CodeName
               ,ProcedureCodeName
               ,DefaultValues
               ,TotalCount
               ,RowNumber)
            SELECT
                rd.PrimaryKey
                ,rd.CategoryName
                ,rd.CodeName
                ,rd.ProcedureCodeName
                ,rd.DefaultValues
                , @TotalRow AS TotalCount
                , ROW_NUMBER() OVER ( ORDER BY CASE WHEN @SortExpression = 'ProcedureCodeName' THEN rd.ProcedureCodeName END
						    , CASE WHEN @SortExpression = 'ProcedureCodeName desc' THEN rd.ProcedureCodeName END DESC
						    , CASE WHEN @SortExpression = 'CodeName' THEN rd.CodeName END
						    , CASE WHEN @SortExpression = 'CodeName desc' THEN rd.CodeName END DESC
						    , CASE WHEN @SortExpression = 'CategoryName' THEN rd.CategoryName END
						    , CASE WHEN @SortExpression = 'CategoryName desc' THEN rd.CategoryName END desc
						, rd.PrimaryKey ) AS RowNumber
                FROM
                    #RawData AS rd;
                         
            SELECT TOP ( CASE WHEN ( @PageNumber = -1 ) THEN @TotalRow
                              ELSE ( @PageSize )
                         END )
                  rrs.PrimaryKey
                 ,rrs.CategoryName
                 ,rrs.CodeName
                 ,rrs.ProcedureCodeName
                 ,rrs.DefaultValues
                 ,rrs.TotalCount
                 ,rrs.RowNumber
                INTO
                    #FinalResultSet
                FROM
                    #RankResultSet AS rrs
                WHERE
                    RowNumber > ( ( @PageNumber - 1 ) * @PageSize );               
              
            IF NOT EXISTS ( SELECT 1 FROM #FinalResultSet )
                BEGIN               
                    SELECT
                            0 AS PageNumber ,
                            0 AS NumberOfPages ,
                            0 NumberOfRows;               
                END;               
            ELSE
                BEGIN               
                    SELECT TOP 1
                            @PageNumber AS PageNumber ,
                            CASE ( TotalCount % @PageSize )
                              WHEN 0 THEN ISNULL(( TotalCount / @PageSize ) , 0)
                              ELSE ISNULL(( TotalCount / @PageSize ) , 0) + 1
                            END AS NumberOfPages ,
                            ISNULL(TotalCount , 0) AS NumberOfRows
                        FROM
                            #FinalResultSet;               
                END;               
              
              SELECT
                    TexasProviderValidationId = PrimaryKey
                    ,ProcedureCodeName 
                    ,CategoryName
                    ,CodeName
                    ,DefaultValues
                FROM
                    #FinalResultSet
                ORDER BY
                    RowNumber;               
              
                
        END TRY               
              
        BEGIN CATCH               
            DECLARE @Error VARCHAR(8000);               
              
            SET @Error = CONVERT(VARCHAR , ERROR_NUMBER()) + '*****' + CONVERT(VARCHAR(4000) , ERROR_MESSAGE())
                + '*****' + ISNULL(CONVERT(VARCHAR , ERROR_PROCEDURE()) , 'bbtcsp_ListPageCustomTexasProcedureCodeValidations')
                + '*****' + CONVERT(VARCHAR , ERROR_LINE()) + '*****' + CONVERT(VARCHAR , ERROR_SEVERITY()) + '*****'
                + CONVERT(VARCHAR , ERROR_STATE());               
              
            RAISERROR ( @Error,-- Message text.                                           
                    16,-- Severity.                                           
                    1 -- State.                                           
        );               
        END CATCH;               
    END; 

GO