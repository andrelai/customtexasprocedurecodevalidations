
IF OBJECT_ID('dbo.bbtcsp_ValidateCustomTexasProcedureCodeValidations') IS NOT NULL
	DROP PROCEDURE dbo.bbtcsp_ValidateCustomTexasProcedureCodeValidations
GO

 CREATE PROCEDURE [dbo].[bbtcsp_ValidateCustomTexasProcedureCodeValidations]      
  @CurrentUserId INT,
 @ScreenKeyId INT

AS  
   
/******************************************************************************************  
* Stored Procedure: [bbtcsp_ValidateCustomTexasProcedureCodeValidations]            
*       Date			Author                  Purpose          
-------------------------------------------------------------------------------------------  
*	2020-01-31			dbo			 			
******************************************************************************************/

      
Begin                                                      
                
  Begin try    
   
   DECLARE @IncomingCodeName varchar(250)
   DECLARE @IncomingCategory varchar(250)
   DECLARE @IncomingProcedureCodeName varchar(250)
   DECLARE @IncomingCategoryName varchar(250)
   
   SELECT 
     @IncomingCodeName = ctpcv.CodeName
     ,@IncomingCategory = ctpcv.CategoryName
     ,@IncomingProcedureCodeName = ctpcv.ProcedureCodeName
     ,@IncomingCategoryName = CASE ctpcv.CategoryName WHEN 'xServiceRecipients' THEN 'Service Recipients'
                                       WHEN 'xModeOfDelivery' THEN 'Mode of Delivery'
                                       WHEN 'xCrisisType' THEN 'Crisis'
              end
   FROM CustomTexasProcedureCodeValidations as ctpcv
   WHERE
     @ScreenKeyId = TexasProviderValidationId

   DECLARE @ValidationReturnTable TABLE (        
      TableName varchar(200),            
      ColumnName varchar(200),    
      ErrorMessage varchar(1000)      
     )  


  IF EXISTS (        
        SELECT 1 
        FROM CustomTexasProcedureCodeValidations
        WHERE  ISNULL(RecordDeleted, 'N') = 'N' 
        AND CodeName = @IncomingCodeName
        and CategoryName = @IncomingCategory
        and ProcedureCodeName = @IncomingProcedureCodeName
        AND @ScreenKeyId != TexasProviderValidationId
      )
  begin
      INSERT INTO @ValidationReturnTable (
      TableName
     ,ColumnName
     ,ErrorMessage)
    VALUES ('CustomTexasProcedureCodeValidations'
    , 'AdjustmentCodeId'
    , 'The Procedure Code-Custom Field Type-Allowed Option combination already exists.');
  END

  if NOT exists (
    select 1
    from CustomTexasProcedureCodeValidations as ctpcv
    join GlobalCodes as gc on gc.Category = ctpcv.CategoryName
    WHERE
     @ScreenKeyId = TexasProviderValidationId
     and ctpcv.CodeName = gc.CodeName
  )
   begin
      INSERT INTO @ValidationReturnTable (
      TableName
     ,ColumnName
     ,ErrorMessage)
    VALUES ('CustomTexasProcedureCodeValidations'
    , 'CodeName'
    , 'The allowed option "' + @IncomingCodeName +  '" is not acceptable for the category "' + @IncomingCategoryName +'"');

  END

  Select TableName, ColumnName, ErrorMessage         
  from @validationReturnTable      
      
  IF Exists (Select * From @validationReturnTable)      
  Begin       
   select 1  as ValidationStatus      
  End      
  Else      
  Begin      
   Select 0 as ValidationStatus      
  End     


  end try                                                          
                                                                                          
  BEGIN CATCH              
            
  DECLARE @Error varchar(8000)                                                           
  SET @Error= Convert(varchar,ERROR_NUMBER()) + '*****' + Convert(varchar(4000),ERROR_MESSAGE())                                                                                    
      + '*****' + isnull(Convert(varchar,ERROR_PROCEDURE()),'[bbtcsp_ValidateCustomTexasProcedureCodeValidations]')
      + '*****' + Convert(varchar,ERROR_LINE()) + '*****' + Convert(varchar,ERROR_SEVERITY()) 
      + '*****' + Convert(varchar,ERROR_STATE())                                  
      RAISERROR                                                                                         
   (                                                           
    @Error, -- Message text.                                                                                        
    16, -- Severity.                                                                                        
    1 -- State.                                                                                        
   );                                                                                     
  END CATCH                                    
END               
GO


