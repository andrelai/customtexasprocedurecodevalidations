BEGIN TRAN

DECLARE @FormNameList VARCHAR(250) = 'Custom Texas Procedure Code Validations'
DECLARE @FormNameDetail VARCHAR(250) = 'Custom Texas Procedure Code Validations Details'
DECLARE @FormCollectionIdList INT
DECLARE @FormCollectionIdDetail INT
DECLARE @ScreenIdList INT
DECLARE @ScreenIdDetail int
DECLARE @ListPageColumnConfigurationId int
DECLARE @FormIdList INT = (SELECT TOP 1 f.FormId FROM Forms AS f WHERE f.FormName = @FormNameList AND ISNULL(f.RecordDeleted, 'N') = 'N' ORDER BY f.FormId)
DECLARE @FormIdDetail INT = (SELECT TOP 1 f.FormId FROM Forms AS f WHERE f.FormName = @FormNameDetail AND ISNULL(f.RecordDeleted, 'N') = 'N' ORDER BY f.FormId)
DECLARE @ListPageColumnConfigurationColumnLinkId int

-- form collection insert -- list page
INSERT INTO FormCollections (
 NumberOfForms
 ,CollectionType
 ,FormCollectionName)
SELECT 
  1
  ,11126769
  ,f.FormName
FROM Forms AS f
LEFT JOIN FormCollectionForms AS fcf ON f.FormId = fcf.FormId AND ISNULL(f.RecordDeleted, 'N') = 'N'
WHERE f.FormName = @FormNameList
AND fcf.FormCollectionFormId IS null

SET @FormCollectionIdList = (SELECT TOP 1 fc.FormCollectionId FROM FormCollections AS fc WHERE fc.FormCollectionName = @FormNameList AND ISNULL(fc.RecordDeleted, 'N') = 'N')

-- form collection insert -- detail page
INSERT INTO FormCollections (
 NumberOfForms
 ,CollectionType
 ,FormCollectionName)
SELECT 
  1
  ,11126771
  ,f.FormName
FROM Forms AS f
LEFT JOIN FormCollectionForms AS fcf ON f.FormId = fcf.FormId AND ISNULL(f.RecordDeleted, 'N') = 'N'
WHERE f.FormName = @FormNameDetail
AND fcf.FormCollectionFormId IS null

SET @FormCollectionIdDetail = (SELECT TOP 1 fc.FormCollectionId FROM FormCollections AS fc WHERE fc.FormCollectionName = @FormNameDetail AND ISNULL(fc.RecordDeleted, 'N') = 'N')

-- form colleciton form insert -- list
INSERT INTO FormCollectionForms (
 FormCollectionId
 ,FormId
 ,Active
 ,FormOrder)
SELECT fc.FormCollectionId, @FormIdList, 'Y', 1
FROM FormCollections AS fc
LEFT JOIN FormCollectionForms AS fcf ON fc.FormCollectionId = fcf.FormCollectionId AND ISNULL(fc.RecordDeleted, 'N') = 'N'
WHERE fc.FormCollectionId = @FormCollectionIdList


SELECT * FROM FormCollections WHERE FormCollectionId = @FormCollectionIdList
SELECT * FROM FormCollectionForms AS fcf WHERE fcf.FormCollectionId = @FormCollectionIdList

-- form colleciton form insert -- detail
INSERT INTO FormCollectionForms (
 FormCollectionId
 ,FormId
 ,Active
 ,FormOrder)
SELECT fc.FormCollectionId, @FormIdDetail, 'Y', 1
FROM FormCollections AS fc
LEFT JOIN FormCollectionForms AS fcf ON fc.FormCollectionId = fcf.FormCollectionId AND ISNULL(fc.RecordDeleted, 'N') = 'N'
WHERE fc.FormCollectionId = @FormCollectionIdDetail


SELECT * FROM FormCollections WHERE FormCollectionId = @FormCollectionIdDetail
SELECT * FROM FormCollectionForms AS fcf WHERE fcf.FormCollectionId = @FormCollectionIdDetail

-- Screen insert -- list
INSERT INTO Screens (
 ScreenName
 ,ScreenType
 ,ScreenURL
 ,TabId)
SELECT fc.FormCollectionName
  ,5762
  ,'/CommonUserControls/DFAListPage.ascx'
  ,4 --admin
FROM FormCollections AS fc
WHERE fc.FormCollectionId = @FormCollectionIdList

SELECT TOP 1 @ScreenIdList =ScreenId FROM Screens WHERE ScreenName = @FormNameList AND ISNULL(RecordDeleted, 'N') = 'N' ORDER BY ScreenId ASC

SELECT * FROM Screens WHERE ScreenId = @ScreenIdList


-- Screen insert -- detail
INSERT INTO Screens (
 ScreenName
 ,ScreenType
 ,ScreenURL
 ,TabId
 ,InitializationStoredProcedure
 ,ValidationStoredProcedureUpdate
 ,PostUpdateStoredProcedure
 ,Code)
SELECT fc.FormCollectionName
  ,5761
  ,'/CommonUserControls/DFAMultitabDetailPage.ascx'
  ,4 --admin
  , null
  ,'bbtcsp_ValidateCustomTexasProcedureCodeValidations'
  , null
  ,'TexasProviderValidationId'
FROM FormCollections AS fc
WHERE fc.FormCollectionId = @FormCollectionIdDetail

SELECT TOP 1 @ScreenIdDetail =ScreenId FROM Screens WHERE ScreenName = @FormNameDetail AND ISNULL(RecordDeleted, 'N') = 'N' ORDER BY ScreenId ASC

SELECT * FROM Screens WHERE ScreenId = @ScreenIdDetail

-- update list form with detail screen
UPDATE f
SET DetailScreenId = @ScreenIdDetail
FROM Forms AS f
WHERE FormId = @FormIdList

-- screen object collection -- list

INSERT INTO ScreenObjectCollections (
 ScreenId
 ,FormCollectionId)
SELECT s.ScreenId, @FormCollectionIdList
FROM Screens AS s
LEFT JOIN ScreenObjectCollections AS soc ON s.ScreenId = soc.ScreenId AND FormCollectionId = @FormCollectionIdList AND ISNULL(s.RecordDeleted, 'N') = 'N'
WHERE s.ScreenId = @ScreenIdList

SELECT * FROM ScreenObjectCollections AS soc WHERE soc.ScreenId = @ScreenIdList AND soc.FormCollectionId = @FormCollectionIdList

-- screen object collection -- detail

INSERT INTO ScreenObjectCollections (
 ScreenId
 ,FormCollectionId)
SELECT s.ScreenId, @FormCollectionIdDetail
FROM Screens AS s
LEFT JOIN ScreenObjectCollections AS soc ON s.ScreenId = soc.ScreenId AND FormCollectionId = @FormCollectionIdDetail AND ISNULL(s.RecordDeleted, 'N') = 'N'
WHERE s.ScreenId = @ScreenIdDetail

SELECT * FROM ScreenObjectCollections AS soc WHERE soc.ScreenId = @ScreenIdDetail AND soc.FormCollectionId = @FormCollectionIdDetail


--- list page configurations
INSERT INTO ListPageColumnConfigurations (
 ScreenId
 ,ViewName
 ,Active
 ,DefaultView
 ,Template)
SELECT s.ScreenId, 'Original', 'Y', null, 'Y'
FROM Screens AS s 
LEFT JOIN ListPageColumnConfigurations AS lpcc ON s.ScreenId = lpcc.ScreenId
WHERE s.ScreenId = @ScreenIdList
AND lpcc.ListPageColumnConfigurationId IS null

SELECT @ListPageColumnConfigurationId = ListPageColumnConfigurationId 
FROM ListPageColumnConfigurations 
WHERE ScreenId = @ScreenIdList

SELECT *
FROM ListPageColumnConfigurations AS lpcc
WHERE lpcc.ListPageColumnConfigurationId = @ListPageColumnConfigurationId


INSERT INTO ListPageColumnConfigurationColumns (
 ListPageColumnConfigurationId
 ,FieldName
 ,Caption
 ,DisplayAs
 ,SortOrder
 ,ShowColumn
 ,Width
 ,Fixed)
 VALUES (@ListPageColumnConfigurationId ,'TexasProviderValidationId', 'TexasProviderValidationId' , 'TexasProviderValidationId', 1, 'N', 150, 'N')
,(@ListPageColumnConfigurationId ,'ProcedureCodeName', 'Procedure Code Name' , 'Procedure Code Name', 1, 'Y', 75, 'N')
,(@ListPageColumnConfigurationId ,'CategoryName', 'Custom Field Type' , 'Custom Field Type', 2, 'Y', 75, 'N')
,(@ListPageColumnConfigurationId ,'CodeName', 'Allowed Option' , 'Allowed Option', 3, 'Y', 75, 'N')
,(@ListPageColumnConfigurationId ,'DefaultValues', 'Default Values' , 'Default Values', 5, 'Y', 50, 'N')

 
SELECT * FROM ListPageColumnConfigurationColumns AS lpccc 
WHERE @ListPageColumnConfigurationId = lpccc.ListPageColumnConfigurationId

INSERT INTO ListPageColumnConfigurationColumnLinks (
  ListPageColumnConfigurationColumnId
 ,ScreenId
 ,PopUp)
SELECT lpccc.ListPageColumnConfigurationColumnId, @ScreenIdDetail, 'N'
FROM ListPageColumnConfigurationColumns AS lpccc
LEFT JOIN ListPageColumnConfigurationColumnLinks AS lpcccl ON lpcccl.ScreenId = @ScreenIdDetail
WHERE lpccc.FieldName = 'ProcedureCodeName'
AND lpccc.ListPageColumnConfigurationId = @ListPageColumnConfigurationId
AND ISNULL(lpccc.RecordDeleted, 'N') = 'N'
AND lpcccl.ListPageColumnConfigurationColumnLinkId IS null


SELECT @ListPageColumnConfigurationColumnLinkId = lpcccl.ListPageColumnConfigurationColumnLinkId FROM ListPageColumnConfigurationColumnLinks AS lpcccl WHERE lpcccl.ScreenId = @ScreenIdDetail

SELECT * FROM ListPageColumnConfigurationColumnLinks WHERE @ListPageColumnConfigurationColumnLinkId = ListPageColumnConfigurationColumnLinkId

INSERT INTO ListPageColumnConfigurationColumnLinkParameters (
 ListPageColumnConfigurationColumnLinkId
 ,ListPageColumnConfigurationColumnId
 ,KeyColumn)
SELECT @ListPageColumnConfigurationColumnLinkId, lpccc.ListPageColumnConfigurationColumnId, 'Y' FROM ListPageColumnConfigurationColumns AS lpccc
WHERE lpccc.FieldName = 'TexasProviderValidationId'
AND lpccc.ListPageColumnConfigurationId = @ListPageColumnConfigurationId


SELECT * FROM ListPageColumnConfigurationColumnLinkParameters WHERE @ListPageColumnConfigurationColumnLinkId = ListPageColumnConfigurationColumnLinkId



commit

GO

