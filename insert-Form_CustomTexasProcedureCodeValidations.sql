

BEGIN TRY
  BEGIN TRANSACTION
  DECLARE @DFAJavaScript VARCHAR(MAX)
  SET @DFAJavaScript = ''''
  DECLARE @FormIds TABLE (
    FormId INT
  )
  DECLARE @FormId INT
         ,@FormSectionId INT
         ,@FormSectionGroupId INT
  DECLARE @NewFormSection TABLE (
    NewFormSectionId INT NOT NULL
   ,OldFormSectionId INT NOT NULL
  )
  DECLARE @NewFormSectionGroup TABLE (
    NewFormSectionGroupId INT NOT NULL
   ,OldFormSectionGroupId INT NOT NULL
  )
  INSERT INTO forms (
    formname
   ,TableName
   ,TotalNumberOfColumns
   ,Active
   ,RetrieveStoredProcedure
   ,FormType
   ,FormJavascript
   ,IsJavascriptOverride
   ,Core) OUTPUT INSERTED.FormId INTO @FormIds
  VALUES ('Custom Texas Procedure Code Validations', null, 1, 'Y', 'bbtcsp_ListPageCustomTexasProcedureCodeValidations', '9470', @DFAJavaScript, 'Y', 'N')
  SET @FormId = (SELECT TOP 1
      FormId
    FROM @FormIds)
  INSERT INTO dbo.FormSections (
    formid
   ,SortOrder
   ,PlaceOnTopOfPage
   ,SectionLabel
   ,Active
   ,SectionEnableCheckBox
   ,SectionEnableCheckBoxText
   ,SectionEnableCheckBoxColumnName
   ,NumberOfColumns) OUTPUT INSERTED.FormSectionId, '90313' INTO @NewFormSection
  VALUES (@FormId, 1, 'Y', 'Filters', 'Y', 'N', NULL, NULL, 1)
  INSERT INTO dbo.FormSectionGroups (
    formsectionid
   ,SortOrder
   ,GroupLabel
   ,Active
   ,GroupEnableCheckBox
   ,GroupEnableCheckBoxText
   ,GroupEnableCheckBoxColumnName
   ,NumberOfItemsInRow
   ,GroupName) OUTPUT INSERTED.FormSectionGroupId, '93106' INTO @NewFormSectionGroup
  VALUES ((SELECT TOP 1
      NewFormSectionId
    FROM @NewFormSection
    WHERE OldFormSectionId = 90313)
  , 1, NULL, 'Y', 'N', NULL, NULL, '3', 'Group 1')
  INSERT INTO Formitems (
    formsectionid
   ,FormSectionGroupId
   ,ItemType
   ,ItemLabel
   ,SortOrder
   ,active
   ,GlobalCodeCategory
   ,ItemColumnName
   ,ItemRequiresComment
   ,ItemCommentColumnName
   ,ItemWidth
   ,MaximumLength
   ,DropdownType
   ,SharedTableName
   ,StoredProcedureName
   ,ValueField
   ,TextField
   ,MultilineEditFieldHeight
   ,EachRadioButtonOnNewLine
   ,FilterName
   ,[Filter])
  VALUES ((SELECT TOP 1
      NewFormSectionId
    FROM @NewFormSection
    WHERE OldFormSectionId = 90313)
  , (SELECT TOP 1
      NewFormSectionGroupId
    FROM @NewFormSectionGroup
    WHERE OldFormSectionGroupId = 93106)
  , 5361, 'Procedure Code Name', 5, 'Y', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,'ProcedureCodeName','Y')
  INSERT INTO Formitems (
    formsectionid
   ,FormSectionGroupId
   ,ItemType
   ,ItemLabel
   ,SortOrder
   ,active
   ,GlobalCodeCategory
   ,ItemColumnName
   ,ItemRequiresComment
   ,ItemCommentColumnName
   ,ItemWidth
   ,MaximumLength
   ,DropdownType
   ,SharedTableName
   ,StoredProcedureName
   ,ValueField
   ,TextField
   ,MultilineEditFieldHeight
   ,EachRadioButtonOnNewLine
   ,FilterName
   ,[Filter])
  VALUES ((SELECT TOP 1
      NewFormSectionId
    FROM @NewFormSection
    WHERE OldFormSectionId = 90313)
  , (SELECT TOP 1
      NewFormSectionGroupId
    FROM @NewFormSectionGroup
    WHERE OldFormSectionGroupId = 93106)
  , 5372, 'Category', 10, 'Y', NULL, NULL, 'N', NULL, 150, NULL, 'S', NULL, 'bbtcsp_GetCustomFieldsGlobalCodeCategory', 'Category', 'CategoryName', NULL, NULL,'Category','Y')
  COMMIT TRANSACTION;
END TRY
BEGIN CATCH
  IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;
  DECLARE @ErrorMessage NVARCHAR(MAX)
  DECLARE @ErrorSeverity INT
  DECLARE @ErrorState INT
  SET @ErrorMessage = ERROR_MESSAGE()
  SET @ErrorSeverity = ERROR_SEVERITY()
  SET @ErrorState = ERROR_STATE()
  RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH 