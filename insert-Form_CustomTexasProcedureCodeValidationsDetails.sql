BEGIN TRY
  BEGIN TRANSACTION
  DECLARE @DFAJavaScript VARCHAR(MAX)
  SET @DFAJavaScript = 'function ValidateCustomPageEventHandler()
{
  if ($("[id$=DropDownList_CustomTexasProcedureCodeValidations_ProcedureCodeName]").val() == "" || $("[id$=DropDownList_CustomTexasProcedureCodeValidations_ProcedureCodeName]").val() == null)
  {
    ShowHideErrorMessage("Procedure Code is required", "true");
    return false;
  }
  else if ($("[id$=DropDownList_CustomTexasProcedureCodeValidations_CategoryName]").val() == "" || $("[id$=DropDownList_CustomTexasProcedureCodeValidations_CategoryName]").val() == null)
  {
    ShowHideErrorMessage("Custom Field Type is required", "true");
    return false;
  }
  
  else if ($("[id$=DropDownList_CustomTexasProcedureCodeValidations_CodeName]").val() == "" || $("[id$=DropDownList_CustomTexasProcedureCodeValidations_CodeName]").val() == null)
  {
    ShowHideErrorMessage("Allowed Option is required", "true");
    return false;
  }

  else if ($("input[name=''RadioButton_CustomTexasProcedureCodeValidations_DefaultValues'']:checked").val() == "" || $("input[name=''RadioButton_CustomTexasProcedureCodeValidations_DefaultValues'']:checked").val() == null) 
  {
    ShowHideErrorMessage("Default Value is required", "true");
    return false;
  }

  else if ($("input[name=''RadioButton_CustomTexasProcedureCodeValidations_AllowComplete'']:checked").val() == "" || $("input[name=''RadioButton_CustomTexasProcedureCodeValidations_AllowComplete'']:checked").val() == null)
  {
    ShowHideErrorMessage("Allow services with Procedure-Custom field option to complete is required", "true");
    return false;
  }
  else {
    return true;
  }

}'
  DECLARE @FormIds TABLE (
    FormId INT
  )
  DECLARE @FormId INT
         ,@FormSectionId INT
         ,@FormSectionGroupId INT
  DECLARE @NewFormSection TABLE (
    NewFormSectionId INT NOT NULL
   ,OldFormSectionId INT NOT NULL
  )
  DECLARE @NewFormSectionGroup TABLE (
    NewFormSectionGroupId INT NOT NULL
   ,OldFormSectionGroupId INT NOT NULL
  )
  INSERT INTO forms (
    formname
   ,TableName
   ,TotalNumberOfColumns
   ,Active
   ,RetrieveStoredProcedure
   ,FormType
   ,FormJavascript
   ,IsJavascriptOverride
   ,Core) OUTPUT INSERTED.FormId INTO @FormIds
  VALUES ('Custom Texas Procedure Code Validations Details', 'CustomTexasProcedureCodeValidations', 1, 'Y', '', '9468', @DFAJavaScript, 'Y', 'N')
  SET @FormId = (SELECT TOP 1
      FormId
    FROM @FormIds)
  INSERT INTO dbo.FormSections (
    formid
   ,SortOrder
   ,PlaceOnTopOfPage
   ,SectionLabel
   ,Active
   ,SectionEnableCheckBox
   ,SectionEnableCheckBoxText
   ,SectionEnableCheckBoxColumnName
   ,NumberOfColumns) OUTPUT INSERTED.FormSectionId, '90314' INTO @NewFormSection
  VALUES (@FormId, 1, NULL, ' Procedure Code Validation', 'Y', NULL, NULL, NULL, 1)
  INSERT INTO dbo.FormSectionGroups (
    formsectionid
   ,SortOrder
   ,GroupLabel
   ,Active
   ,GroupEnableCheckBox
   ,GroupEnableCheckBoxText
   ,GroupEnableCheckBoxColumnName
   ,NumberOfItemsInRow
   ,GroupName) OUTPUT INSERTED.FormSectionGroupId, '93107' INTO @NewFormSectionGroup
  VALUES ((SELECT TOP 1
      NewFormSectionId
    FROM @NewFormSection
    WHERE OldFormSectionId = 90314)
  , 1, NULL, 'Y', 'N', NULL, NULL, '2', 'Group 1')
  INSERT INTO Formitems (
    formsectionid
   ,FormSectionGroupId
   ,ItemType
   ,ItemLabel
   ,SortOrder
   ,active
   ,GlobalCodeCategory
   ,ItemColumnName
   ,ItemRequiresComment
   ,ItemCommentColumnName
   ,ItemWidth
   ,MaximumLength
   ,DropdownType
   ,SharedTableName
   ,StoredProcedureName
   ,ValueField
   ,TextField
   ,MultilineEditFieldHeight
   ,EachRadioButtonOnNewLine)
  VALUES ((SELECT TOP 1
      NewFormSectionId
    FROM @NewFormSection
    WHERE OldFormSectionId = 90314)
  , (SELECT TOP 1
      NewFormSectionGroupId
    FROM @NewFormSectionGroup
    WHERE OldFormSectionGroupId = 93107)
  , '5372', '', 10, 'Y', NULL, 'ProcedureCodeName', 'N', NULL, NULL, NULL, 'S', NULL, 'ssp_SCGetProcedureCodes', 'ProcedureCodeName', 'ProcedureCodeName', NULL, NULL)
  INSERT INTO Formitems (
    formsectionid
   ,FormSectionGroupId
   ,ItemType
   ,ItemLabel
   ,SortOrder
   ,active
   ,GlobalCodeCategory
   ,ItemColumnName
   ,ItemRequiresComment
   ,ItemCommentColumnName
   ,ItemWidth
   ,MaximumLength
   ,DropdownType
   ,SharedTableName
   ,StoredProcedureName
   ,ValueField
   ,TextField
   ,MultilineEditFieldHeight
   ,EachRadioButtonOnNewLine)
  VALUES ((SELECT TOP 1
      NewFormSectionId
    FROM @NewFormSection
    WHERE OldFormSectionId = 90314)
  , (SELECT TOP 1
      NewFormSectionGroupId
    FROM @NewFormSectionGroup
    WHERE OldFormSectionGroupId = 93107)
  , '5374', 'Procedure Code', 5, 'Y', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
  INSERT INTO Formitems (
    formsectionid
   ,FormSectionGroupId
   ,ItemType
   ,ItemLabel
   ,SortOrder
   ,active
   ,GlobalCodeCategory
   ,ItemColumnName
   ,ItemRequiresComment
   ,ItemCommentColumnName
   ,ItemWidth
   ,MaximumLength
   ,DropdownType
   ,SharedTableName
   ,StoredProcedureName
   ,ValueField
   ,TextField
   ,MultilineEditFieldHeight
   ,EachRadioButtonOnNewLine)
  VALUES ((SELECT TOP 1
      NewFormSectionId
    FROM @NewFormSection
    WHERE OldFormSectionId = 90314)
  , (SELECT TOP 1
      NewFormSectionGroupId
    FROM @NewFormSectionGroup
    WHERE OldFormSectionGroupId = 93107)
  , '5372', '', 20, 'Y', NULL, 'CategoryName', 'N', NULL, NULL, NULL, 'S', NULL, 'bbtcsp_GetCustomFieldsGlobalCodeCategory', 'Category', 'CategoryName', NULL, NULL)
  INSERT INTO Formitems (
    formsectionid
   ,FormSectionGroupId
   ,ItemType
   ,ItemLabel
   ,SortOrder
   ,active
   ,GlobalCodeCategory
   ,ItemColumnName
   ,ItemRequiresComment
   ,ItemCommentColumnName
   ,ItemWidth
   ,MaximumLength
   ,DropdownType
   ,SharedTableName
   ,StoredProcedureName
   ,ValueField
   ,TextField
   ,MultilineEditFieldHeight
   ,EachRadioButtonOnNewLine)
  VALUES ((SELECT TOP 1
      NewFormSectionId
    FROM @NewFormSection
    WHERE OldFormSectionId = 90314)
  , (SELECT TOP 1
      NewFormSectionGroupId
    FROM @NewFormSectionGroup
    WHERE OldFormSectionGroupId = 93107)
  , '5374', 'Custom Field Type', 15, 'Y', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
  INSERT INTO Formitems (
    formsectionid
   ,FormSectionGroupId
   ,ItemType
   ,ItemLabel
   ,SortOrder
   ,active
   ,GlobalCodeCategory
   ,ItemColumnName
   ,ItemRequiresComment
   ,ItemCommentColumnName
   ,ItemWidth
   ,MaximumLength
   ,DropdownType
   ,SharedTableName
   ,StoredProcedureName
   ,ValueField
   ,TextField
   ,MultilineEditFieldHeight
   ,EachRadioButtonOnNewLine)
  VALUES ((SELECT TOP 1
      NewFormSectionId
    FROM @NewFormSection
    WHERE OldFormSectionId = 90314)
  , (SELECT TOP 1
      NewFormSectionGroupId
    FROM @NewFormSectionGroup
    WHERE OldFormSectionGroupId = 93107)
  , '5374', 'Default Value', 25, 'Y', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
  INSERT INTO Formitems (
    formsectionid
   ,FormSectionGroupId
   ,ItemType
   ,ItemLabel
   ,SortOrder
   ,active
   ,GlobalCodeCategory
   ,ItemColumnName
   ,ItemRequiresComment
   ,ItemCommentColumnName
   ,ItemWidth
   ,MaximumLength
   ,DropdownType
   ,SharedTableName
   ,StoredProcedureName
   ,ValueField
   ,TextField
   ,MultilineEditFieldHeight
   ,EachRadioButtonOnNewLine)
  VALUES ((SELECT TOP 1
      NewFormSectionId
    FROM @NewFormSection
    WHERE OldFormSectionId = 90314)
  , (SELECT TOP 1
      NewFormSectionGroupId
    FROM @NewFormSectionGroup
    WHERE OldFormSectionGroupId = 93107)
  , '5365', '', 30, 'Y', 'RADIOYN             ', 'DefaultValues', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
  INSERT INTO Formitems (
    formsectionid
   ,FormSectionGroupId
   ,ItemType
   ,ItemLabel
   ,SortOrder
   ,active
   ,GlobalCodeCategory
   ,ItemColumnName
   ,ItemRequiresComment
   ,ItemCommentColumnName
   ,ItemWidth
   ,MaximumLength
   ,DropdownType
   ,SharedTableName
   ,StoredProcedureName
   ,ValueField
   ,TextField
   ,MultilineEditFieldHeight
   ,EachRadioButtonOnNewLine)
  VALUES ((SELECT TOP 1
      NewFormSectionId
    FROM @NewFormSection
    WHERE OldFormSectionId = 90314)
  , (SELECT TOP 1
      NewFormSectionGroupId
    FROM @NewFormSectionGroup
    WHERE OldFormSectionGroupId = 93107)
  , '5374', 'Allowed Option', 21, 'Y', NULL, NULL, 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
  INSERT INTO Formitems (
    formsectionid
   ,FormSectionGroupId
   ,ItemType
   ,ItemLabel
   ,SortOrder
   ,active
   ,GlobalCodeCategory
   ,ItemColumnName
   ,ItemRequiresComment
   ,ItemCommentColumnName
   ,ItemWidth
   ,MaximumLength
   ,DropdownType
   ,SharedTableName
   ,StoredProcedureName
   ,ValueField
   ,TextField
   ,MultilineEditFieldHeight
   ,EachRadioButtonOnNewLine)
  VALUES ((SELECT TOP 1
      NewFormSectionId
    FROM @NewFormSection
    WHERE OldFormSectionId = 90314)
  , (SELECT TOP 1
      NewFormSectionGroupId
    FROM @NewFormSectionGroup
    WHERE OldFormSectionGroupId = 93107)
  , '5372', '', 23, 'Y', NULL, 'CodeName', 'N', NULL, NULL, NULL, 'S', NULL, 'bbtcsp_GetCustomFieldsCodeName', 'CodeName', 'CategoryName', NULL, NULL)
  COMMIT TRANSACTION;
END TRY
BEGIN CATCH
  IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;
  DECLARE @ErrorMessage NVARCHAR(MAX)
  DECLARE @ErrorSeverity INT
  DECLARE @ErrorState INT
  SET @ErrorMessage = ERROR_MESSAGE()
  SET @ErrorSeverity = ERROR_SEVERITY()
  SET @ErrorState = ERROR_STATE()
  RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH 