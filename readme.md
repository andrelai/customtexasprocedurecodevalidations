# Steps to get list page and detail page for Custom Texas Procedure Code Validations
1. deploy sql scripts (see Deployment order)
1. open newly created screens "Custom Texas Procedure Code Validations" and "Custom Texas Procedure Code Validations Details" and "Save"
1. refresh shared tables
1. add banner that links to Custom Texas Procedure Code Validations (list page)
1. grant permission to banner "Custom Texas Procedure Code Validations" and screen "Custom Texas Procedure Code Validations Details"
1. Log out and back in
1. On the list page, hover mouse over to the eye icon without the "+" sign and select original.

# SQL Script Deployment order
1. bbtcsp_ListPageCustomTexasProcedureCodeValidations.sql
1. bbtcsp_GetCustomFieldsGlobalCodeCategory.sql
1. bbtcsp_GetCustomFieldsCodeName
1. bbtcsp_ValidateCustomTexasProcedureCodeValidations.sql
1. insert-Form_CustomTexasProcedureCodeValidations.sql
1. insert-Form_CustomTexasProcedureCodeValidationsDetails.sql
1. insert-FormCollectionsScreensListPageColumnConfigs.sql
1. update-CustomTexasProcedureCodeValidations_CategoryName_To_UpperCase.sql - this modifies CategoryName column to camel casing consistent for use with this module as the detail screen requires casing to be exact for 2 way binding.

# Dependencies
1. ssp_SCGetProcedureCodes

# Known Issues
1. Sorting of the list page depends to be somewhat erratic
1. Red error when record deleting from detail page: "Index was out of range. Must be non-negative and less than the size of the collection. Parameter name: index". This seems to be a bug but does not actually affect the record deletion.
1. In the detail page, some procedure codes are appearing as blank in the drop down: This is caused by the usage of ProcedureCodeName as the reference in CustomTexasProcedureCodeValidations instead of ProcedureCodeId, any discrepancies between the ProcedureCodeName in ProcedureCodes table and CustomTexasProcedureCodeValidations will cause the binding to fail and give blanks. Correcting the broken reference will address this issue.